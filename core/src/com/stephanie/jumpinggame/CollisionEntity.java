package com.stephanie.jumpinggame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Jon on 19/03/2017.
 * The CollisionEntity is for any object with a collision box, platforms or collectibles.
 * This is a kinematic entity therefore not effected by gravity but can be moved
 */

class CollisionEntity {
    protected Sprite entity;
    protected BodyDef bodyDefinition;
    protected Body body;

    public CollisionEntity(Texture image, World world, float x, float y, float density, boolean isTrigger) {
//      The entity is the Sprite drawn to the screen, this has no relation to the body but will follow the bodies coordinates
        entity = new Sprite(image);
//      Sets the initial position of the sprite
        entity.setPosition(x, y);

//      BodyDef defines the type of body, for the jumper this is a kinematic body, therefor allowing
//      it to be moved in the world physics simulation not effected by gravity.
        bodyDefinition = new BodyDef();
        bodyDefinition.type = BodyDef.BodyType.KinematicBody;
        bodyDefinition.position.set((entity.getX() + entity.getWidth() / 2) / JumpingGame.PIXELS_PER_METER,
                (entity.getY() + entity.getHeight() / 2) / JumpingGame.PIXELS_PER_METER);
        bodyDefinition.fixedRotation = true;

        body = world.createBody(bodyDefinition);

        body.setUserData(this);

//      Now define the dimensions of the physics shape
        PolygonShape shape = new PolygonShape();
//      Basically set the physics polygon to a box with the same dimensions as our sprite
        shape.setAsBox(entity.getWidth() / 2 / JumpingGame.PIXELS_PER_METER, entity.getHeight() / 2 / JumpingGame.PIXELS_PER_METER);

//      This is where you, in addition to defining the shape of the body
//      you also define it's properties like density and restitution
//      If you are wondering, density and area are used to calculate over all mass
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = density;
        fixtureDef.isSensor = isTrigger;

        body.createFixture(fixtureDef);
        shape.dispose();
    }

    public BodyDef getBodyDefinition()
    {
        return bodyDefinition;
    }

    public Sprite getSprite() {
        return entity;
    }

    public void setPosition(float x, float y) { entity.setPosition(x, y); }

    public float getX() {
        return entity.getX();
    }

    public float getY() {
        return entity.getY();
    }

    public Body getBody() { return body; }

    /**
     * Updates the entity, stuff that you think should go in the renderer to do something to the
     * jumper should go here.
     */
    public void update() {
        entity.setPosition(body.getPosition().x * JumpingGame.PIXELS_PER_METER - entity.getWidth() / 2,
                body.getPosition().y * JumpingGame.PIXELS_PER_METER - entity.getHeight() / 2);
    }

    public void dispose(World world) {
        if (world != null)
            world.destroyBody(body);
    }
}
