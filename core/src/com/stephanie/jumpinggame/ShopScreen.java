package com.stephanie.jumpinggame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

/**
 * Created by Jon on 15/05/2017.
 * Displays the shop/character customization screen
 * Character selection and purchasing
 * Hat selection and purchasing
 * {@link #show()}
 * {@link #render(float)}
 */

class ShopScreen extends JumpingGameScreen {

    private int currentEquippedSkin;
    private int currentEquippedHat;

    public ShopScreen(JumpingGame gam) {
        super(gam);

//      Get stored user data
        currentEquippedSkin = 0;
        currentEquippedHat = 0;

        final Table tableMain = new Table(game.skin); //Where everything goes
        final Label shopLabel = new Label("Shop", game.skin); //Top label to indicate screen
        final Label currency = new Label("100", game.skin); //Top right to indicate your value
        final Label skinsLabel = new Label("Skins", game.skin); //Label above skins scroller
        final Label hatsLabel = new Label("Hats", game.skin); //Label above hats scroller
        final TextButton skinsButton = new TextButton("Equipped", game.skin, "default");
        final TextButton hatsButton = new TextButton("Equipped", game.skin, "default");
        Texture backTexture = new Texture("data/back_icon.png");
        final ImageButton returnButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(backTexture)));

        final Table skinsScrollTable = new Table(game.skin); //Holds all owned/purchasable skins
        final ButtonGroup<ImageButton> skinIcons = new ButtonGroup<ImageButton>();
        final Table hatsScrollTable = new Table(game.skin); //Holds all owned/purchasable hats
        final ButtonGroup<ImageButton> hatIcons = new ButtonGroup<ImageButton>();

        final ScrollPane skinsScrollPane = new ScrollPane(skinsScrollTable, game.skin);
        final ScrollPane hatsScrollPane = new ScrollPane(hatsScrollTable, game.skin);

        /*========================================================================================
        Scroll tables
        ==========================================================================================*/
        int imageScale = 2;
        TextureRegionDrawable selectTexture = new TextureRegionDrawable(new TextureRegion(new Texture("data/selected.png")));

//      Skins Table
        for (int i = 0; i < game.characters.size(); i++) {
            TextureRegionDrawable characterImage = new TextureRegionDrawable(new TextureRegion(game.characters.get(i)));

            final ImageButton skinButton = new ImageButton(characterImage, characterImage, characterImage.tint(Color.CYAN));
            if (i == currentEquippedSkin)
                skinButton.setChecked(true);

            skinButton.getImageCell().size(game.characters.get(i).getWidth() * imageScale, game.characters.get(i).getHeight() * imageScale);

            skinIcons.add(skinButton);

            skinButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor){
                    int buttonIndex = skinIcons.getCheckedIndex();

                    if (buttonIndex == currentEquippedSkin)
                        skinsButton.getLabel().setText("Equipped");
                    else {
//                      TODO: Check if skin is owned and name the button "Buy <amount>"
                        skinsButton.getLabel().setText("Equip");
                    }
                }
            });

            skinsScrollTable.add(skinButton);
        }
        skinIcons.setMaxCheckCount(1);
        skinIcons.setMinCheckCount(1);

        skinsScrollTable.setFillParent(true);
        skinsScrollPane.setScrollingDisabled(false, true);

        skinsButton.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (skinsButton.getLabel().getText().toString().equals("Equip")) {
                    skinsButton.getLabel().setText("Equipped");
                    game.character = skinIcons.getCheckedIndex();
                    currentEquippedSkin = skinIcons.getCheckedIndex();
                }else if (skinsButton.getLabel().getText().toString().substring(0, 3).equals("Buy")) {
//                  TODO: Purchase the skin
                    skinsButton.getLabel().setText("Equipped");
                    game.character = skinIcons.getCheckedIndex();
                    currentEquippedSkin = skinIcons.getCheckedIndex();
                }
            }
        });

//      Hats Table
        for (int i = 0; i < game.hats.size() + 1; i++) {
            final ImageButton hatButton;
            if (i == 0) {
                Texture blankHat = new Texture("data/blank_hat.png");
                TextureRegionDrawable hatImage = new TextureRegionDrawable(new TextureRegion(blankHat));
                hatButton = new ImageButton(hatImage, hatImage, hatImage.tint(Color.CYAN));
                hatButton.getImageCell().size(blankHat.getWidth() * imageScale, blankHat.getHeight() * imageScale);
            }else {
                TextureRegionDrawable hatImage = new TextureRegionDrawable(new TextureRegion(game.hats.get(i - 1)));
                hatButton = new ImageButton(hatImage, hatImage, hatImage.tint(Color.CYAN));
                hatButton.getImageCell().size(game.hats.get(i - 1).getWidth() * imageScale, game.hats.get(i - 1).getHeight() * imageScale);
            }
            if (i == currentEquippedHat)
                hatButton.setChecked(true);

            hatIcons.add(hatButton);

            hatButton.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor){
                    int buttonIndex = hatIcons.getCheckedIndex();

                    if (buttonIndex == currentEquippedHat)
                        hatsButton.getLabel().setText("Equipped");
                    else {
//                      TODO: Check if hat is owned
                        hatsButton.getLabel().setText("Equip");
                    }
                }
            });

            hatsScrollTable.add(hatButton);
        }
        hatIcons.setMaxCheckCount(1);
        hatIcons.setMinCheckCount(1);

        hatsScrollTable.setFillParent(true);
        hatsScrollPane.setScrollingDisabled(false, true);

        hatsButton.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (hatsButton.getLabel().getText().toString().equals("Equip")) {
                    hatsButton.getLabel().setText("Equipped");
                    game.hat = hatIcons.getCheckedIndex() - 1;
                    currentEquippedHat = hatIcons.getCheckedIndex();
                }else if (skinsButton.getLabel().getText().toString().substring(0, 3).equals("Buy")) {
//                  TODO: Purchase the hat
                    hatsButton.getLabel().setText("Equipped");
                    game.hat = hatIcons.getCheckedIndex() - 1;
                    currentEquippedHat = hatIcons.getCheckedIndex();
                }
            }
        });

//      Return Button
        imageScale = 3;
        returnButton.getImageCell().size(backTexture.getWidth() * imageScale, backTexture.getHeight() * imageScale);
        returnButton.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button){
                game.setScreen(game.mainMenuScreen);
                Gdx.app.log("Clicked", "clicked");
            }
        });

//      Labels
        shopLabel.setFontScale(imageScale);
        currency.setFontScale(imageScale);
        skinsLabel.setFontScale(imageScale);
        hatsLabel.setFontScale(imageScale);

//      Buttons
        skinsButton.getLabel().setFontScale(imageScale);
        hatsButton.getLabel().setFontScale(imageScale);

        /*========================================================================================
        Main screen table
        =========================================================================================*/
        tableMain.setFillParent(true);
        tableMain.pad(10);
        tableMain.top();

        tableMain.row().padBottom(200).expandX();
        tableMain.add(returnButton).align(Align.left);
        tableMain.add(shopLabel);
        tableMain.add(currency).align(Align.right);

        tableMain.row();
        tableMain.add(skinsLabel).colspan(3).align(Align.left);
        tableMain.row().padBottom(50);
        tableMain.add(skinsScrollPane).colspan(3).fill();
        tableMain.row().padBottom(100);
        tableMain.add(skinsButton).colspan(3).size(game.BUTTON_MAIN_WIDTH / 2, game.BUTTON_MAIN_HEIGHT / 2);

        tableMain.row();
        tableMain.add(hatsLabel).colspan(3).align(Align.left);
        tableMain.row().padBottom(50);
        tableMain.add(hatsScrollPane).colspan(3).fill();
        tableMain.row().padBottom(100);
        tableMain.add(hatsButton).colspan(3).size(game.BUTTON_MAIN_WIDTH / 2, game.BUTTON_MAIN_HEIGHT / 2);

//        tableMain.setDebug(true);

        screenStage.addActor(tableMain);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(screenStage);
        currentEquippedSkin = game.character;
        currentEquippedHat = game.hat + 1;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        screenStage.draw();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}
