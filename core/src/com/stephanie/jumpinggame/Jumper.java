package com.stephanie.jumpinggame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Jon on 19/03/2017.
 * {@link #jump(float)}
 * {@link #update()}
 */

class Jumper {
    private Sprite character, hat;
    private BodyDef bodyDefinition;
    private Body body;
    private boolean isJumping, isContacting, fallen;
    private float jumpVelocity, time, jumpTime, jumpStartX, jumpEndX;

    /**
     * Constructor for a new jumper
     * @param image The image drawn to the screen (the skin)
     * @param world The simulation world
     * @param ground The ground that the character initially stands on
     */
    public Jumper(Texture image, World world, CollisionEntity ground) {
//      The character is the Sprite drawn to the screen, this has no relation to the body but will follow the bodies coordinates
        character = new Sprite(image);

//      Sets the initial position of the sprite
        character.setPosition(Sides.MIDDLE - character.getWidth() / 2, Sides.BOTTOM + ground.getSprite().getHeight() + 5);

//      BodyDef defines the type of body, for the jumper this is a dynamic body, therefor allowing
//      it to be moved in the world physics simulation also be effected by gravity.
        bodyDefinition = new BodyDef();
        bodyDefinition.type = BodyDef.BodyType.DynamicBody;
        bodyDefinition.position.set((character.getX() + character.getWidth()/2) / JumpingGame.PIXELS_PER_METER,
                (character.getY() + character.getHeight()/2) / JumpingGame.PIXELS_PER_METER);
        bodyDefinition.fixedRotation = true;

        body = world.createBody(bodyDefinition);
//        body.setLinearDamping(1f);
//      Links the body to the jumper, used for getting this fixture when contacting with a platform.
        body.setUserData(this);

//      PolygonShape is used to define the dimensions of the body
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(character.getWidth() / 2 / JumpingGame.PIXELS_PER_METER,
                character.getHeight() / 2 / JumpingGame.PIXELS_PER_METER);

//      This is where you, in addition to defining the shape of the body
//      you also define it's properties like density and restitution
//      If you are wondering, density and area are used to calculate over all mass
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 5f;

        body.createFixture(fixtureDef);
        shape.dispose();


        isJumping = false;
        isContacting = true;
        fallen = false;
        jumpVelocity = 10;
        time = 1f;
        jumpTime = 1f;
        jumpStartX = body.getPosition().x / JumpingGame.PIXELS_PER_METER;
        jumpEndX = 0;
    }

    /**
     * Sets the body of the character's velocity
     * @param x The target location to jump to along the x axis
     */
    public void jump(float x) {
        if (!isJumping) {
            isJumping = true;
            jumpStartX = body.getPosition().x * JumpingGame.PIXELS_PER_METER;
            jumpEndX = x;

            float yVel = jumpVelocity;
            float xVel = ((jumpEndX - jumpStartX) / JumpingGame.PIXELS_PER_METER) / time;

            body.setLinearVelocity(new Vector2(xVel, yVel));
        }
    }

    public Sprite getSprite() {
        return character;
    }

    public void setPosition(float x, float y) { character.setPosition(x, y); }

    public float getX() {
        return character.getX();
    }

    public float getY() {
        return character.getY();
    }

    public Body getBody() { return body; }

    /**
     * Updates the character
     */
    public void update() {
//      Body positions are from the center of the body
        float bodyX = body.getPosition().x * JumpingGame.PIXELS_PER_METER;
        float bodyY = body.getPosition().y * JumpingGame.PIXELS_PER_METER;
//      Sprites are drawn from bottom left to top right
        float charWidth = character.getWidth() / 2;
        float charHeight = character.getHeight() / 2;
        if (!isJumping || bodyX + charWidth >= Sides.RIGHT || bodyX - charWidth <= Sides.LEFT)
            body.setLinearVelocity(0, body.getLinearVelocity().y);

//      Updates the sprites position based on the bodies position
        character.setPosition(bodyX - charWidth, bodyY - charHeight);
        if (hat != null) {
            hat.setPosition(bodyX - charWidth, bodyY - charHeight);
        }

//      Character has fallen below the screen
        if (bodyY + charHeight < Sides.BOTTOM)
            fallen = true;
    }

    /**
     * Sets if the player is in the air or not
     * @param jumping boolean, true for airborne, false for landed
     */
    public void setJumping(boolean jumping) {
        this.isJumping = jumping;
    }

    public boolean getJumping() { return isJumping; }

    public void resetTime() {
        time = 0;
    }

    /**
     * Sets if the jumper is inside a platform
     * @param contacting
     */
    public void setContacting(boolean contacting) {
        this.isContacting = contacting;
    }

    public boolean getContacting() {
        return isContacting;
    }

    public void setHat(Texture hat) {
        this.hat = new Sprite(hat);
        this.hat.setPosition(character.getX(), character.getY());
    }

    public Sprite getHat() {
        return hat;
    }

    public boolean hasFallen() {
        return fallen;
    }

    public void dispose(World world) {
        if (world != null)
            world.destroyBody(body);
        character.getTexture().dispose();
    }
}

/**
 * This listener is used to detect when two bodies collide
 * It is used mainly for platform and jumper collision but should also be used for hazards and
 * collectibles
 */
class JumperCollisionListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {

    }

    @Override
    public void endContact(Contact contact) {

    }

    /**
     * This method is ran before a collision occurs
     * Its used to detect when the character has landed on the ground and decides when the character
     * should pass through a platform
     * @param contact This holds the two fixtures that are about to contact
     * @param oldManifold
     */
    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
//      Gets both fixtures in the collision
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        Body jumper; //jumper body
        Body other; //other body (use this for hazards)

        if (fixA != null && fixA.getBody().getUserData().getClass() == Jumper.class) {
            jumper = fixA.getBody();
            other = fixB.getBody();
        }else if (fixB != null && fixB.getBody().getUserData().getClass() == Jumper.class) {
            jumper = fixB.getBody();
            other = fixA.getBody();
        }else {
//          If a collision occurred between two other bodies
            return;
        }

//      sets the height of the jumper's feet and top of the platform body
        float jumperHeight = jumper.getPosition().y * JumpingGame.PIXELS_PER_METER - ((Jumper)jumper.getUserData()).getSprite().getHeight() / 2;
        float otherHeight = other.getPosition().y * JumpingGame.PIXELS_PER_METER + ((CollisionEntity)other.getUserData()).getSprite().getHeight() / 2;

        if (jumperHeight <= otherHeight) { //is the jumper under the platform
            contact.setEnabled(false);
            ((Jumper)jumper.getUserData()).setContacting(true);
        }else { //enables contact if above the platform
            contact.setEnabled(true);
            ((Jumper)jumper.getUserData()).setContacting(false);
        }

//      gets if the player has landed on the platform (within a small range above the platform
        if ((jumperHeight - otherHeight < 5) && (jumperHeight - otherHeight > -2) && (jumper.getLinearVelocity().y < 0)) {
            ((Jumper) jumper.getUserData()).setJumping(false);
        }
    }

    /**
     * Doesn't appear to get called, has been tested
     * @param contact
     * @param impulse
     */
    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}