package com.stephanie.jumpinggame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Jon on 26/03/2017.
 */

class Collectible extends CollisionEntity{
    public Collectible(Texture image, World world, float density, boolean trigger) {
        super(image, world, Sides.LEFT, Sides.BOTTOM, density, trigger);
        body.setUserData(this);
    }
}
