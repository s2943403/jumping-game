package com.stephanie.jumpinggame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Jon on 13/05/2017.
 * Displays the game screen and runs the game
 * {@link #GameScreen(JumpingGame) Game screen}
 * {@link #show()}
 * {@link #render(float)}
 * {@link #stepWorld(boolean)}
 */
class GameScreen extends JumpingGameScreen {
    private World world; //All simulation is done here
    private boolean step;

    //	Entities/Sprites
    private CollisionEntity groundEntity;
    private Jumper jumper;
    private ArrayList<Platform> platformEntities;
    private Sprite[] backgrounds;

    //	Gravity, increases with time
    private float gravity = -20f;

    private float fallSpeed = -0.5f; //Speed at which the platforms fall

    private boolean started; //Has the player started jumping and playing the game
    private float elapsedTime; //total time the player has played for (local not delta)
    private float secondTimer; //the time for the score to increase
    private static final float TIME_STEP = 1f/60f; //how much does the game progress in seconds
    private float timeIncrease = 1f; //The time for the score to increment

    private float timeToNextPlatform; //How much time has passed until a platform is generated
    private float maxTimeForNextPlatform; //How long until another platform is generated
    private float minTimeForNextPlatform; //Minimum required time before another platform
    private float timeForNextPlatform; //The required time to generate a platform

    private int jumpHeight; //A generic value not linked to the character jump height but for platforms
    private int minHeight; //the minimum height that a platform will generate at
    private int zone; //ground = 0, sky = 2, space = 4; used to get the platform textures

    private int score; //your score
    private Label scoreLabel; //what displays the score

    private Random rand;

//  Used for drawing debug lines around the bodies
    private Box2DDebugRenderer debugRenderer;
    private Matrix4 debugMatrix;

    public GameScreen(JumpingGame gam) {
        super(gam);

        /*==========================================================
         Menu Elements
        ============================================================*/
        scoreLabel = new Label("0", game.skin, "default");
        Texture pauseButtonTexture = new Texture("data/pause_icon.png");
        final ImageButton pauseButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(pauseButtonTexture)));

        int pauseButtonScale = 3;
        int padding = 10;
        pauseButton.getImage().setScale(pauseButtonScale);
        pauseButton.setPosition(Sides.RIGHT - pauseButtonTexture.getWidth() * pauseButtonScale - padding,
                Sides.TOP - pauseButtonTexture.getHeight() * pauseButtonScale - padding);

        pauseButton.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button){
                game.setScreen(game.mainMenuScreen);
            }
        });


        scoreLabel.setFontScale(5);
        scoreLabel.setPosition(Sides.LEFT + padding, Sides.TOP - scoreLabel.getHeight() - padding, Align.left);

        screenStage.addActor(pauseButton);
        screenStage.addActor(scoreLabel);

        screenStage.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button){
                if (!started)
                    started = true;
                Vector2 screenCoords = screenStage.stageToScreenCoordinates(new Vector2(x, y));
//                Gdx.app.log("Touch Coords", screenCoords.x + ", " + screenCoords.y);
                Vector3 worldCoords = camera.unproject(new Vector3(screenCoords.x, screenCoords.y, 0));
//                Gdx.app.log("World Coords", worldCoords.x + ", " + worldCoords.y);
                jumper.jump(worldCoords.x);
            }
        });

//        debugRenderer = new Box2DDebugRenderer();

        step = false;
    }

    @Override
    public void show() {
//      Sets the input processor to this screen's stage
        Gdx.input.setInputProcessor(screenStage);
        /*========================================================================================
         Initialises the world so each play through is random
         =========================================================================================*/
        world = new World(new Vector2(0, gravity), true);
//		Adds the jumper collision listener to the world
        world.setContactListener(new JumperCollisionListener());

//      Creates a new ground
        groundEntity = new CollisionEntity(game.ground, world, Sides.LEFT, Sides.BOTTOM, 1f, false);

//      Creates a new jumper
        jumper = new Jumper(game.characters.get(game.character), world, groundEntity);
//      Sets a hat if one was selected from the menu
        if (game.hat >= 0)
            jumper.setHat(game.hats.get(game.hat));

//		Platform Generation
        platformEntities = new ArrayList<Platform>();

        minHeight = game.characters.get(game.character).getHeight() + game.platforms[0].getHeight();
        jumpHeight = 300;

        int minSide = 0;
        int maxSide = 2;

        float height = Sides.BOTTOM;

        boolean broken = false;

        rand = new Random();

        do {
            int heightIncrement = rand.nextInt(jumpHeight - minHeight + 1) + minHeight;
            height += heightIncrement;
            if (height >= Sides.TOP)
                break;

            if (height > Sides.TOP) {
                height = Sides.TOP;
            }

            int randSide = rand.nextInt(maxSide - minSide + 1) + minSide;
            float side = 0;
            switch (randSide) {
                case 0: side = Sides.LEFT; break;
                case 1: side = Sides.MIDDLE; break;
                case 2: side = Sides.RIGHT; break;
                default: side = 0f;
            }

            broken = platformEntities.size() != 0 && Math.random() < 0.1 && !platformEntities.get(platformEntities.size() - 1).isBroken();

            Platform tempPlatform = new Platform(game.platforms[0], world, 1f, broken);
            if (randSide == 1)
                tempPlatform.generate(side, height + (game.platforms[0].getHeight()), 0, broken, game.platformMiddle[(broken ? 1 : 0)]);
            else
                tempPlatform.generate(side, height + (game.platforms[0].getHeight()), 0, broken, game.platforms[(broken ? 1 : 0)]);
            platformEntities.add(tempPlatform);

            if (broken) {
                height -= heightIncrement + game.platforms[0].getHeight();
            }
        }while (height < Sides.TOP);

//      Loads the background sprites into memory not completed
//        backgrounds = new Sprite[game.backgrounds.length];
//        for (int i = 0; i < game.backgrounds.length; i++) {
//            backgrounds[i] = new Sprite(game.backgrounds[i]);
//        }
//      TODO: make background scroll with platforms - sprites cannot my moved with a velocity value
//      Solution 1: make the background a physical object and place into the world
//      Solution 2: Use velocity to calculate the distance traveled in this time frame with the
//      travel distance being from the top of the screen to the bottom



//		debugRenderer = new Box2DDebugRenderer();
        started = false;
        elapsedTime = 0f;
        fallSpeed = -0.5f;

        timeToNextPlatform = 0f;
        maxTimeForNextPlatform = jumpHeight / game.PIXELS_PER_METER / -fallSpeed;
        minTimeForNextPlatform = minHeight / game.PIXELS_PER_METER / -fallSpeed;
        timeForNextPlatform = rand.nextFloat() *
                (maxTimeForNextPlatform - minTimeForNextPlatform) + minTimeForNextPlatform;;

        zone = 0;
        score = 0;
        secondTimer = 0;

        scoreLabel.setText(score + "");

        step = true;
    }

    /**
     * Renders the screen stage
     * @param delta Time from last frame
     */
    @Override
    public void render(float delta) {
        camera.update();
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stepWorld(step);

        game.batch.setProjectionMatrix(camera.combined);

//        debugMatrix = game.batch.getProjectionMatrix().cpy().scale(JumpingGame.PIXELS_PER_METER,
//                JumpingGame.PIXELS_PER_METER, 0);

//		Updates all drawables for each stage
        game.batch.begin();
        if (groundEntity != null)
            game.batch.draw(groundEntity.getSprite(), groundEntity.getX(), groundEntity.getY());
		for (Platform p : platformEntities) {
			if (p.isOnScreen())
                game.batch.draw(p.getSprite(), p.getX(), p.getY());
			else
				break;
		}

        game.batch.draw(jumper.getSprite(), jumper.getX(), jumper.getY());
        if (game.hat >= 0)
            game.batch.draw(jumper.getHat(), jumper.getX(), jumper.getY());
        game.batch.end();
        screenStage.draw();

//        debugRenderer.render(world, debugMatrix);
    }

    /**
     * Updates the world and all components within
     * @param doStep should the game continue simulating
     */
    private void stepWorld(boolean doStep) {
        if (!doStep) return;

        //If the game has started, increment timers and test for zone changes
        if (started) {
            elapsedTime += TIME_STEP;
            secondTimer += TIME_STEP;
            if (score == 100) {
                zone = 2;
            }else if (score == 200) {
                zone = 4;
            }
        }

//      Advances the world at a rate of 60hz
        world.step(TIME_STEP, 6, 1);

//		Updates the jumpers position
        jumper.update();

//      Stops the game when the player has fallen off the screen
        if (jumper.hasFallen()) {
//			TODO: Open game over screen
            started = false;
            step = false;
            for (Platform p : platformEntities)
                p.getBody().setLinearVelocity(0, 0);
        }

//      Updates the ground's velocity and sprite location if still on the screen
        if (groundEntity != null) {
            if (started)
                groundEntity.getBody().setLinearVelocity(0, fallSpeed);
            groundEntity.update();
//          Removes the ground entity when it falls below the screen as it is no longer needed.
            if (groundEntity.getBody().getPosition().y * JumpingGame.PIXELS_PER_METER +
                    groundEntity.getSprite().getHeight() / 2 < Sides.BOTTOM) {
                groundEntity.dispose(world);
                groundEntity = null;
            }
        }

//      Deals with platform falling, time based platform generation.
        if (started && timeToNextPlatform >= timeForNextPlatform) {
            Platform p = platformEntities.get(platformEntities.size() - 1); //Gets the platform at the end of the list
//          Checks if that platform is still on the screen.
            if (p.isOnScreen()) {
//              This section of code executes if the game requires a new instance of a platform
                int minSide = 0;
                int maxSide = 2;

                int randSide = rand.nextInt(maxSide - minSide + 1) + minSide;
                float side = 0;
                switch (randSide) {
                    case 0: side = Sides.LEFT; break;
                    case 1: side = Sides.MIDDLE; break;
                    case 2: side = Sides.RIGHT; break;
                    default: side = 0f;
                }
//              Decides if the platform should be broken with a 10% chance and if the prior platform is broken
                boolean broken = Math.random() < 0.1 && !platformEntities.get(0).isBroken();

                p = new Platform(game.platforms[0], world, 1f, broken);
                if (randSide == 1)
                    p.generate(side, Sides.TOP + (game.platforms[0].getHeight()), 0, broken, game.platformMiddle[zone + (broken ? 1 : 0)]);
                else
                    p.generate(side, Sides.TOP + (game.platforms[0].getHeight()), 0, broken, game.platforms[zone + (broken ? 1 : 0)]);
                platformEntities.add(p); //Adds the platform at the end of the list up the top of the screen
            }else {
//              Will move the platform onscreen if it is otherwise off the screen
                int minSide = 0;
                int maxSide = 2;

                int randSide = rand.nextInt(maxSide - minSide + 1) + minSide;
                float side = 0;
                switch (randSide) {
                    case 0: side = Sides.LEFT; break;
                    case 1: side = Sides.MIDDLE; break;
                    case 2: side = Sides.RIGHT; break;
                    default: side = 0f;
                }

                boolean broken = Math.random() < 0.1 && !platformEntities.get(0).isBroken();

                if (randSide == 1)
                    p.generate(side, Sides.TOP + (game.platforms[0].getHeight()), 0, broken, game.platformMiddle[zone + (broken ? 1 : 0)]);
                else
                    p.generate(side, Sides.TOP + (game.platforms[0].getHeight()), 0, broken, game.platforms[zone + (broken ? 1 : 0)]);
                Collections.rotate(platformEntities, 1); //Rotates the list to keep platforms in order of height
            }
            timeToNextPlatform = 0f;
            timeForNextPlatform = 0f;
        }

//      Updates the platforms positions and velocities
        for (int i = 0; i < platformEntities.size(); i++) {
            if (!platformEntities.get(i).isOnScreen()) //Skips this platform if not on the screen
                continue;

            if (started) {
                platformEntities.get(i).getBody().setLinearVelocity(0, fallSpeed);
            }

            platformEntities.get(i).update();
//          puts the platform in reserve if it is off the screen
            if (!platformEntities.get(i).isOnScreen()) {
                platformEntities.get(i).reserve();
                platformEntities.add(platformEntities.remove(i)); //TODO: Test game without this line
            }
        }

//		Increments the falling velocity  and score based on the time step constant 60hz
        if (started) {
            fallSpeed -= TIME_STEP / 60f;
            timeToNextPlatform += TIME_STEP;
            if (timeForNextPlatform == 0f) {
                if (!platformEntities.get(0).isBroken()) {
                    maxTimeForNextPlatform = jumpHeight / game.PIXELS_PER_METER / -fallSpeed;
                    minTimeForNextPlatform = minHeight / game.PIXELS_PER_METER / -fallSpeed;
                }else {
                    maxTimeForNextPlatform = minHeight / game.PIXELS_PER_METER / -fallSpeed;
                    minTimeForNextPlatform = 0f;
                }

                timeForNextPlatform = rand.nextFloat() *
                        (maxTimeForNextPlatform - minTimeForNextPlatform) + minTimeForNextPlatform;
                timeToNextPlatform = 0f;
            }

            if (secondTimer >= timeIncrease) {
                score++;
                scoreLabel.setText(score + "");
                secondTimer = 0;

                if (timeIncrease >= TIME_STEP)
                    timeIncrease -= TIME_STEP/30;
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        step = false;
    }

    @Override
    public void dispose() {
        super.dispose();
        world.dispose();
    }
}
