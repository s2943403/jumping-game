package com.stephanie.jumpinggame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

/**
 * Created by Jon on 13/05/2017.
 * Displays the main menu screen
 * Play Button
 * Shop Button
 * Settings sub menu
 * {@link #show()}
 * {@link #render(float)}
 * {@link #dispose()}
 */

class MainMenuScreen extends JumpingGameScreen {
    private Texture settingsTexture;
    public MainMenuScreen(JumpingGame gam) {
        super(gam);
        final TextButton playButton = new TextButton("Play", game.skin, "default"); //Takes user to the game
        final TextButton shopButton = new TextButton("Shop", game.skin, "default"); //Takes user to the shop

        settingsTexture = new Texture("data/settings_icon.png");
        final ImageButton settingsButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(settingsTexture)));
        final Label musicVolumeLabel = new Label("Music:", game.skin, "default");
        final CheckBox musicCheckBox = new CheckBox("", game.skin);
        final Label effectVolumeLabel = new Label("Sound Effects:", game.skin, "default");
        final CheckBox effectCheckBox = new CheckBox("", game.skin);
        final TextButton rateButton = new TextButton("Rate the App", game.skin, "default");
        final Table settingsMenuTable = new Table(game.skin);

        playButton.setWidth(game.BUTTON_MAIN_WIDTH);
        playButton.setHeight(game.BUTTON_MAIN_HEIGHT);
        playButton.setPosition(Sides.MIDDLE - game.BUTTON_MAIN_WIDTH / 2, Sides.TOP - 400f - game.BUTTON_MAIN_HEIGHT);
        playButton.getLabel().setFontScale(4);
        playButton.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button){
                if (!settingsMenuTable.isVisible())
                    game.setScreen(game.gameScreen);
            }
        });

        shopButton.setWidth(game.BUTTON_MAIN_WIDTH);
        shopButton.setHeight(game.BUTTON_MAIN_HEIGHT);
        shopButton.setPosition(Sides.MIDDLE - game.BUTTON_MAIN_WIDTH / 2, playButton.getY() - 200f - game.BUTTON_MAIN_HEIGHT);
        shopButton.getLabel().setFontScale(playButton.getLabel().getFontScaleX());
        shopButton.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button){
                if (!settingsMenuTable.isVisible())
                    game.setScreen(game.shopScreen);
            }
        });

        rateButton.setWidth(game.BUTTON_MAIN_WIDTH);
        rateButton.setHeight(game.BUTTON_MAIN_HEIGHT);
        rateButton.getLabel().setFontScale(playButton.getLabel().getFontScaleX());
        rateButton.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button){
//                TODO: Take user to app store rate page.
            }
        });

        musicVolumeLabel.setFontScale(4);
        effectVolumeLabel.setFontScale(4);

        musicCheckBox.setChecked(game.music);
        musicCheckBox.getImage().scaleBy(4);
        musicCheckBox.getLabel().setFontScale(4);
        musicCheckBox.getImageCell().size(musicCheckBox.getLabel().getHeight(), musicCheckBox.getLabel().getHeight());

        effectCheckBox.setChecked(game.effects);
        effectCheckBox.getImage().scaleBy(4);
        effectCheckBox.getLabel().setFontScale(4);
        effectCheckBox.getImageCell().size(effectCheckBox.getLabel().getHeight(), effectCheckBox.getLabel().getHeight());


        musicCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.music = musicCheckBox.isChecked();
            }
        });

        effectCheckBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.music = effectCheckBox.isChecked();
            }
        });

//        settingsMenuTable.setDebug(true);
//        musicCheckBox.setDebug(true);
//        effectCheckBox.setDebug(true);

        float settingButtonPadding = 20f;
        float tableHeight = 1200f;
        settingsMenuTable.setVisible(false);
        settingsMenuTable.setSize(game.SCREEN_WIDTH  - settingButtonPadding * 2, tableHeight);
        settingsMenuTable.setPosition(Sides.LEFT + settingButtonPadding, Sides.TOP - settingsButton.getHeight() - settingButtonPadding - tableHeight);

        Pixmap settingsBackground = new Pixmap((int)settingsMenuTable.getWidth(), (int)settingsMenuTable.getHeight(), Pixmap.Format.RGBA8888);
        settingsBackground.setColor(1, 1, 1, 0.8f);
        settingsBackground.fill();
        settingsMenuTable.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(settingsBackground))));

        settingsMenuTable.add(musicVolumeLabel).expandX().padRight(100).align(Align.right);
        settingsMenuTable.add(musicCheckBox).width(100).padRight(100);
        settingsMenuTable.row().padTop(20);
        settingsMenuTable.add(effectVolumeLabel).expandX().padRight(100).align(Align.right);
        settingsMenuTable.add(effectCheckBox).width(100).padRight(100);
        settingsMenuTable.row();
        settingsMenuTable.add(rateButton).colspan(2).size(game.BUTTON_MAIN_WIDTH, game.BUTTON_MAIN_HEIGHT).padTop(100);

        settingsMenuTable.pack();

        settingsButton.setPosition(Sides.RIGHT - settingsButton.getWidth() - settingButtonPadding, Sides.TOP - settingsButton.getHeight() - settingButtonPadding);
        settingsButton.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button){
                Gdx.app.log("Clicked", "Settings menu should open now");
                settingsMenuTable.setVisible(!settingsMenuTable.isVisible());
            }
        });

        screenStage.addActor(playButton);
        screenStage.addActor(shopButton);
        screenStage.addActor(settingsButton);
        screenStage.addActor(settingsMenuTable);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(screenStage);
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        screenStage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
        settingsTexture.dispose();
    }
}
