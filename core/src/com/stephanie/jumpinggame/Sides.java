package com.stephanie.jumpinggame;

import com.badlogic.gdx.Gdx;

/**
 * Created by Jon on 24/03/2017.
 * Contains the locations of the screen sides (x and y).
 */

class Sides {
    protected static final float LEFT = 0;
    protected static final float TOP = JumpingGame.SCREEN_HEIGHT;
    protected static final float RIGHT = JumpingGame.SCREEN_WIDTH;
    protected static final float BOTTOM = 0;
    protected static final float MIDDLE = JumpingGame.SCREEN_WIDTH / 2; //The center of the screen's x axis
}