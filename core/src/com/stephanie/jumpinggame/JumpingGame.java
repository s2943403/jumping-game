package com.stephanie.jumpinggame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.ArrayList;

/**
 * The main section of the game, runs the application
 * {@link #create()}
 * {@link #loadCharacterTextures()}
 * {@link #loadHatTextures()}
 */
public class JumpingGame extends Game {
//	World variables
	protected Skin skin;
    protected SpriteBatch batch; //All sprites are drawn here

	/*********************************************************
	 * Screens
	 *********************************************************/
	protected MainMenuScreen mainMenuScreen;
	protected GameScreen gameScreen;
	protected ShopScreen shopScreen;

    //	Character/Hat for menu selection
    protected int character;
    protected int hat;

//	Image locations
	private ArrayList<String> characterImages;
	private ArrayList<String> hatImages;

	private String groundImage;
	private String[] platformImage;
	private String[] platformMiddleImage;

//	Textures
    protected ArrayList<Texture> characters; //For menu implementation, load files once for usability
    protected ArrayList<Texture> hats; //For menu implementation, load files once for usability

    protected Texture ground;
    protected Texture[] platforms;
    protected Texture[] platformMiddle;

	protected Texture[] backgrounds;

    protected Texture collectible;

//	Conversion value for converting to or from pixels or meters
	protected static final float PIXELS_PER_METER = 200f;

//	Screen width and height in pixels
	protected static final float SCREEN_WIDTH = 1080f;
	protected static final float SCREEN_HEIGHT = 1920f;
    protected final float BUTTON_MAIN_WIDTH = SCREEN_WIDTH - 100f;
    protected final float BUTTON_MAIN_HEIGHT = 200f;

	protected boolean music = true;
	protected boolean effects = true;

	/**
	 * This is works like a constructor, it is the first thing that runs in the application and
	 * where variables should be initialized
	 */
	@Override
    public void create () {
		/*****************************************
		 * UI Elements and menus
		 *****************************************/
		skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		batch = new SpriteBatch();
        mainMenuScreen = new MainMenuScreen(this);
        gameScreen = new GameScreen(this);

		/*****************************************
		 * Loads all textures to use in the game
		 *****************************************/
		characterImages = new ArrayList<String>();
		hatImages = new ArrayList<String>();

		groundImage = "platforms/ground/ground test.png";

		platformImage = new String[6];
		platformMiddleImage = new String[6];
		backgrounds = new Texture[5];

		platformImage[0] = "platforms/standard/zone1.png";
		platformImage[1] = "platforms/standard/zone1_broken.png";
		platformImage[2] = "platforms/standard/zone2.png";
		platformImage[3] = "platforms/standard/zone2_broken.png";
		platformImage[4] = "platforms/standard/zone3.png";
		platformImage[5] = "platforms/standard/zone3_broken.png";

		platformMiddleImage[0] = "platforms/middle/zone1.png";
		platformMiddleImage[1] = "platforms/middle/zone1_broken.png";
		platformMiddleImage[2] = "platforms/middle/zone2.png";
		platformMiddleImage[3] = "platforms/middle/zone2_broken.png";
		platformMiddleImage[4] = "platforms/middle/zone3.png";
		platformMiddleImage[5] = "platforms/middle/zone3_broken.png";

		backgrounds[0] = new Texture("backgrounds/zone1.png");
		backgrounds[1] = new Texture("backgrounds/zone1_transition.png");
		backgrounds[2] = new Texture("backgrounds/zone2.png");
		backgrounds[3] = new Texture("backgrounds/zone2_transition.png");
		backgrounds[4] = new Texture("backgrounds/zone3.png");

//		Ground Generation
		ground = new Texture(groundImage);

//		Texture Loading
		characters = new ArrayList<Texture>();
		hats = new ArrayList<Texture>();
		platforms = new Texture[6];
		platformMiddle = new Texture[6];
		loadCharacterTextures();
		loadHatTextures();

		/**
		 * Shop Screen
		 * Initialized here as it requires the skins and hats arrays to be filled
		 */
		shopScreen = new ShopScreen(this);

		for (int i = 0; i < 6; i++) {
			platforms[i] = new Texture(platformImage[i]);
			platformMiddle[i] = new Texture(platformMiddleImage[i]);
		}

        character = 0;
        hat = -1;

//		Sets the current screen that is viewed
        this.setScreen(mainMenuScreen);
	}

	/**
	 * Renders the game: Acts as a loop that will update the screen with anything that runs within
	 * screens should be cleared before drawing new elements
	 */
	@Override
	public void render () {
        super.render();
	}

	/**
	 * Loads the character texture files from the skins folder
	 * TODO: Change this to read from data/skins.json
	 */
	private void loadCharacterTextures() {
		String path = "characters/skins/";
		FileHandle[] fileList = Gdx.files.internal(path).list();
		Gdx.app.log("FileHandle Size", fileList.length + "");
		characters.ensureCapacity(fileList.length);
		for (FileHandle file : fileList) {
			String fileName = file.file().getName();
			int imageNumber = Integer.parseInt(fileName.substring(9, fileName.length() - 4)) - 1;
			characters.add(imageNumber, new Texture(path + fileName));
		}
	}

	/**
	 * Loads the hat files from the hats folder in characters
	 * TODO: Use the data/hats.json file for texture loading
	 */
	private void loadHatTextures() {
		String path = "characters/hats/";
		FileHandle[] fileList = Gdx.files.internal(path).list();
		hats.ensureCapacity(fileList.length);
		for (FileHandle file : fileList) {
			String fileName = file.file().getName();
			int imageNumber = Integer.parseInt(fileName.substring(3, fileName.length() - 4)) - 1;
			hats.add(imageNumber, new Texture(path + fileName));
		}
	}

	/**
	 * Clears the memory of all data when the app is closed
	 * TODO: Implement the assets manager to dispose of all assets in one method
	 */
	@Override
	public void dispose () {
        batch.dispose();
		for (Texture character : characters)
			character.dispose();
		characters.clear();
		for (Texture hat : hats)
			hat.dispose();
		for (int i = 0; i < platforms.length; i++) {
			platforms[i].dispose();
			platformMiddle[i].dispose();
		}
		for (Texture background : backgrounds)
			background.dispose();
		hats.clear();
		ground.dispose();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}
}
