package com.stephanie.jumpinggame;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

/**
 * Created by Jon on 13/05/2017.
 */

abstract class JumpingGameScreen implements Screen {
    protected Stage screenStage;
    protected final JumpingGame game;
    protected OrthographicCamera camera; //All resolution scaling is done here
    protected StretchViewport viewport;

    public JumpingGameScreen(JumpingGame gam) {
        this.game = gam;
        camera = new OrthographicCamera();
        viewport = new StretchViewport(game.SCREEN_WIDTH, game.SCREEN_HEIGHT, camera);
        screenStage = new Stage(viewport, game.batch);
    }

    @Override
    public abstract void show();

    @Override
    public abstract void render(float delta);

    @Override
    public void resize(int width, int height) {
//        viewport.update(width, height);
    }

    @Override
    public abstract void pause();

    @Override
    public abstract void resume();

    @Override
    public abstract void hide();

    @Override
    public void dispose() {
        screenStage.dispose();
    }
}
