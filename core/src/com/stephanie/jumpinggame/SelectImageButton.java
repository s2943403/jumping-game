package com.stephanie.jumpinggame;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Created by Jon on 22/05/2017.
 * This class was created to allow the use of the selected texture to be drawn over the buttons when
 * they are checked instead of storing two textures for each skin/hat with a selected border around
 * them.
 * It is however incomplete and never tested.
 */

class SelectImageButton extends ImageButton {
    private Image selectedImage;
    public SelectImageButton(Drawable imageUp, Drawable imageChecked) {
        super(imageUp);
        selectedImage.setDrawable(imageChecked);
    }

    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
//        batch.begin();
//        batch.draw(selectedImage, this.getX(), this.getY());
//        batch.end();
    }
}
