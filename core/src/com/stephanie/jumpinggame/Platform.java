package com.stephanie.jumpinggame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Jon on 19/03/2017.
 * A platform is what the character jumps on
 */

class Platform extends CollisionEntity {
    private boolean isBroken;
    private boolean isOnScreen;
    private boolean hasCollectible;

    /***
     * Initializes the platform
     * @param noraml The standard textures or the platform
     * @param world The physical world
     * @param density The bodies density
     * @param breakable Is the platform breakable
     */
    public Platform(Texture noraml, World world, float density, boolean breakable) {
        super(noraml, world, Sides.LEFT, Sides.BOTTOM, density, breakable);

        this.isBroken = breakable;

        body.setUserData(this);
    }

    /***
     * Will generate this platform on a given side, at a given y level (this should be at the top of
     * the screen after initial generation). Provided with a stage variable and if it is broken will
     * decide what texture it holds (ground, sky, or space) broken or not.
     * @param side Left, middle, right
     * @param y What y level to place the platform
     * @param stage 0, 1, 2 - ground, sky, space. What texture should the sprite draw
     * @param breakable is the platform breakable
     */
    public void generate(float side, float y, int stage, boolean breakable, Texture texture) {
        this.isOnScreen = true;
        this.isBroken = breakable;
        entity.setTexture(texture);
        if (side == Sides.LEFT) {
            side = side + entity.getWidth() / 2;
            if (entity.isFlipX())
                entity.flip(true, false); //flips the entity regardless of current state
//            entity.setFlip(false, false); -- directly sets the flip state
        }else if (side == Sides.MIDDLE) {

        }else {
            side = side - entity.getWidth() / 2;
            if (!entity.isFlipX())
                entity.flip(true, false);
        }
        body.getFixtureList().get(0).setSensor(breakable);
        body.setTransform(side / JumpingGame.PIXELS_PER_METER, y / JumpingGame.PIXELS_PER_METER, 0);
    }

    public void reserve() {
        body.setTransform(Sides.RIGHT + entity.getWidth(), 0f, 0f);
        body.setLinearVelocity(0, 0);
    }

    @Override
    public void update() {
        if (!isOnScreen) {
            return;
        }
        super.update();
        if (body.getPosition().y * JumpingGame.PIXELS_PER_METER + entity.getHeight() / 2 < Sides.BOTTOM) {
            this.isOnScreen = false;
        }
    }

    public boolean isOnScreen() {
        return isOnScreen;
    }

    public boolean isBroken() {
        return isBroken;
    }

    public boolean hasCollectible() {
        return hasCollectible;
    }
}
